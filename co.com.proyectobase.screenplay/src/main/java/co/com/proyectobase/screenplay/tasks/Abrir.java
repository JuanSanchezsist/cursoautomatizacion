package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.util.GoogleHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;

public class Abrir implements Task {
	
	private GoogleHomePage googleHomePage;
	

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(googleHomePage));
		
	}

	public static Abrir elTraductorGoogle() {
		return Tasks.instrumented(Abrir.class);
			}


}
