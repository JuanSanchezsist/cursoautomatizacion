package co.com.proyectobase.screenplay.stepdefinitions;

import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.tasks.Abrir;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class TraductorGoogleStepDefintions {



@Managed(driver="chrome")
private WebDriver hisBrowser;
private Actor rafa = Actor.named ("Rafa");

@Before
public void configuracionInicial() {
	rafa.can(BrowseTheWeb.with(hisBrowser));
}


	@Dado("Que Rafa quiere usar el traductor de Google$")
	public void queRafaQuiereUsarElTraductorDeGoogle() throws Exception {
		rafa.wasAbleTo(Abrir.elTraductorGoogle());
			
	}

	@Cuando("el traduce la palabra table de ingles a español$")
	public void elTraduceLaPalabraTableDeInglesAEspañol()throws Exception {

	}

	@Entonces("el deberia ver la palabra mesa en la pantalla$")
	public void elDeberiaVerLaPalabraMesaEnLaPantalla()throws Exception {
	
	}
	

}
