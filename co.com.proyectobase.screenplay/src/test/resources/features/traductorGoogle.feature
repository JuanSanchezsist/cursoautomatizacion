#Author: your.juan.sanchezsist@gmail.com
#language:es
@tag
Feature: Title of your feature
  I want to use this template for my feature file

  @tag1
  Scenario: Title of your scenario
    Dado Que rafa quiere usar el traductor de Google
    Cuando el traduce la palabra table de ingles a español
    Entonces el deberia ver la palabra mesa en la pantalla

  @tag2
  Scenario Outline: Title of your scenario outline
    Given I want to write a step with <name>
    When I check for the <value> in step
    Then I verify the <status> in step

    Examples: 
      | name  | value | status  |
      | name1 |     5 | success |
      | name2 |     7 | Fail    |
