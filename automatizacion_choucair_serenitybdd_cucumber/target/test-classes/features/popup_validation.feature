#language:es
@Regresion
Característica: Formulario Popup Validation.
  El usuario debe poder regresar al formulario los datos requeridos.
  Cada campo del formulario realiza validaciones de obligatoriedad, longitud y formato,
  el sistema debe presentra las validaciones respectivas para cada campo a traves de un globo informatico.


  @CasoExitoso
  Escenario:Diligenciamiento exitoso del formulario Popup Validation.
  no se presenta  mensaje de validación

    Dado  Autentico en colorlib con usuario "demo" y clave "demo"
    Y Ingreso a la funcionalidad Forms Validation
    Cuando Diligencio Fomulario  popup Validation

      | Required | select | MultipleS1 | MultipleS2 | Url                    | Password1 | Password2 | MinSize | MaxSize | Number | IP          | Date       | DataEarLier |
      | Valor1   | Golf   | Tennis     | Golf       | http://valor1@mail.com | Valor1    | Valor1    | 123456  | 123478  | -99.99 | 200.200.5.4 | 2020-01-22 | 2012/09/12  |

    Entonces Verifico ingreso exitoso

  @CasoAlterno
  Escenario:Diligenciamiento exitoso del formulario Popup Validation.
  no se presenta  mensaje de validación

    Dado  Autentico en colorlib con usuario "demo" y clave "demo"
    Y Ingreso a la funcionalidad Forms Validation
    Cuando Diligencio Fomulario  popup Validation

      | Required | select         | MultipleS1 | MultipleS2 | Url                    | Password1 | Password2 | MinSize | MaxSize | Number | IP          | Date       | DataEarLier |
      |          | Golf           | Tennis     | Golf       | http://valor1@mail.com | Valor1    | Valor1    | 123456  | 123478  | -99.99 | 200.200.5.4 | 2020-01-22 | 2012/09/12  |
      | valor1   | Choose a sport | Tennis     | Golf       | http://valor1@mail.com | Valor1    | Valor1    | 123456  | 123478  | -99.99 | 200.200.5.4 | 2020-01-22 | 2012/09/12  |
    Entonces Varificar que se presente Globo Informativo de validación.



  @CasoFeliz
  Esquema del escenario: :Consultar tabla de clientes CNAME y verificar resultados

    Dado  Consiltar CNAME
    |<Documento>|<TipoDocto>|<Nombre>|<Control Terceros>|

Ejemplos:
  | Documento       | TipoDocto | Nombre         | Control Terceros |
  | 000080000003931 | 3         | PERFORMANCE    | 8                |
  | 000080000004576 | 3         | ELIANA MORALES | 1                |

    Y Ingreso a la funcionalidad Forms Validation
    Cuando Diligencio Fomulario  popup Validation
    Entonces Varificar que se presente Globo Informativo de validación.


