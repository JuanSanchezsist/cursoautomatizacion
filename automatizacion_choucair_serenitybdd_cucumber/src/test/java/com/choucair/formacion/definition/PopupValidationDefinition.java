package com.choucair.formacion.definition;

import com.choucair.formacion.steps.ColorlibFormValidationSteps;
import com.choucair.formacion.steps.PopupValidationSteps;
import cucumber.api.DataTable;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import cucumber.api.java.es.Y;
import net.thucydides.core.annotations.Steps;

import java.util.List;

public class PopupValidationDefinition {

    @Steps
    PopupValidationSteps popupValidationSteps ;
    @Steps
    ColorlibFormValidationSteps colorlibFormValidationSteps;


    @Dado("^Autentico en colorlib con usuario (.*)y clave (.*)$")
    public void autentico_en_colorlib_con_usuario_y_calve(String Usuario, String Clave){
        popupValidationSteps.login_colorlib(Usuario, Clave);
    }
    @Y("^Ingreso a la funcionalidad Forms Validation$")
    public void ingreso_a_la_funcionalidad_Forms_Validation() {
        popupValidationSteps.ingresar_form_validation();

    }

    @Cuando("^Diligencio Formulario popup Validation$")
    public void diligencio_Formulario_Popup_Validation(DataTable dtDatosForm){
        List<List<String>> data = dtDatosForm.raw();
        for (int i=1; i<data.size();i++){
          colorlibFormValidationSteps.diligenciar_popup_datos_tabla(data, i);
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {

            }
        }
    }

    @Entonces("^Verifico ingreso exitoso$")
    public void verifico_ingreso_exitoso()  {
        colorlibFormValidationSteps.verificar_ingreso_formulario_sin_errores();
        colorlibFormValidationSteps.verificar_ingreso_formulario_con_errores();

    }

    @Entonces("^Varificar que se presente Globo Informativo de validación$")
    public void varificar_que_se_presente_Globo_Informativo_de_validación() {
        colorlibFormValidationSteps.verificar_ingreso_formulario_con_errores();

    }

}
