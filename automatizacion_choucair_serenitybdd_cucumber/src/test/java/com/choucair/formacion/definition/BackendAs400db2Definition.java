package com.choucair.formacion.definition;

import com.choucair.formacion.steps.BackendAs400db2Steps;
import cucumber.api.DataTable;
import cucumber.api.java.es.Dado;
import net.thucydides.core.annotations.Steps;

import java.util.List;


public class BackendAs400db2Definition {

    @Steps
    BackendAs400db2Steps backendAs400db2Steps;

    @Dado("^Consultar CNAME$")
    public void consultar_CNAME(DataTable dtDatosPrueba){
        List<List<String>> data = dtDatosPrueba.raw();
        backendAs400db2Steps.Consultar_CNAME(data);

    }

    }



