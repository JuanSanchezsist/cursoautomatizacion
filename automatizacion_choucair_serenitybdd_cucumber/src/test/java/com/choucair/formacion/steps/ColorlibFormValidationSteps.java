package com.choucair.formacion.steps;

import com.choucair.formacion.pageobject.ColorlibFormValidationPage;
import net.thucydides.core.annotations.Step;

import java.util.List;

public class ColorlibFormValidationSteps {


    ColorlibFormValidationPage colorlibFormValidationPage;

@Step
     public void diligenciar_popup_datos_tabla(List<List<String>> data, int id){

    colorlibFormValidationPage.Required(data.get(id).get(0).trim());
    colorlibFormValidationPage.Sport1(data.get(id).get(1).trim());
    colorlibFormValidationPage.Multiple_select(data.get(id).get(2).trim());
    colorlibFormValidationPage.Multiple_select(data.get(id).get(3).trim());
    colorlibFormValidationPage.Url(data.get(id).get(4).trim());
    colorlibFormValidationPage.emaill(data.get(id).get(5).trim());
    colorlibFormValidationPage.Pass1(data.get(id).get(6).trim());
    colorlibFormValidationPage.Pass2(data.get(id).get(7).trim());
    colorlibFormValidationPage.minsize1(data.get(id).get(8).trim());
    colorlibFormValidationPage.maxsize1(data.get(id).get(9).trim());
    colorlibFormValidationPage.number2(data.get(id).get(10).trim());
    colorlibFormValidationPage.ip(data.get(id).get(11).trim());
    colorlibFormValidationPage.date3(data.get(id).get(12).trim());
    colorlibFormValidationPage.past(data.get(id).get(13).trim());
    colorlibFormValidationPage.botonValidate();

}

    @Step
    public void verificar_ingreso_formulario_sin_errores(){
        colorlibFormValidationPage.form_sin_errores();
    }
    @Step
    public void verificar_ingreso_formulario_con_errores(){
        colorlibFormValidationPage.form_con_errores();
    }

}
