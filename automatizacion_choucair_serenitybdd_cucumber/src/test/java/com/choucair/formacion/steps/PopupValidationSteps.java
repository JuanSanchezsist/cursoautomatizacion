package com.choucair.formacion.steps;


import com.choucair.formacion.pageobject.ColorlibLoginPage;
import com.choucair.formacion.pageobject.ColorlibMenuPage;
import net.thucydides.core.annotations.Step;

public class PopupValidationSteps {

        ColorlibLoginPage colorlibLoginPage;
        ColorlibMenuPage colorlibMenuPage;


        @Step
        public void login_colorlib(String strUsuario, String strClave) {
            // Abrir navegador con la url
            colorlibLoginPage.open();
            //ingresar usuatio demo
            //ingresar password demo
            //click boton sign in
            colorlibLoginPage.IngresarDatos(strUsuario, strClave);
            //verificar la autenticacion
            colorlibLoginPage.verificarHome();
        }
        @Step
    public void  ingresar_form_validation(){
            colorlibMenuPage.menuFormValidation();
        }
    }

