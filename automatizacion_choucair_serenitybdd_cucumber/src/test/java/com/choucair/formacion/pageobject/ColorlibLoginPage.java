package com.choucair.formacion.pageobject;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("https://colorlib.com/polygon/metis/login.html")
public class ColorlibLoginPage extends PageObject {


    //campo usuario
    @FindBy(css="input.form-control.top")
    public WebElementFacade txtUsername;
    //campo password
    @FindBy(css="input.form-control.bottom")
    public WebElementFacade txtPassword;
    //boton
    @FindBy(css="button.btn.btn-lg.btn-primary.btn-block")
    public WebElementFacade btnSignIn;
    //label  del home a verificar
    @FindBy(css="#bootstrap-admin-template")
    public WebElementFacade lblHomePpal;

    public void IngresarDatos(String strUsuario, String strClave){
        txtUsername.sendKeys(strUsuario);
        txtPassword.sendKeys(strClave);
        btnSignIn.click();
    }
    public void verificarHome() {
        String labelv ="Bootstrap-Admin-Template";
        String strMensaje = lblHomePpal.getText();
        assertThat(strMensaje, containsString(labelv));
    }
}
