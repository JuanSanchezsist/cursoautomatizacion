package com.choucair.formacion.pageobject;
import static org.hamcrest.Matchers.*;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;


import static org.hamcrest.MatcherAssert.assertThat;


public class ColorlibFormValidationPage {

    //Campo Required
    @FindBy(id="req")
    public WebElementFacade txtRequired;
    //Campo select deporte
    @FindBy(id="sport")
    public WebElementFacade cmbSport1;
    //Campo  Url
    @FindBy(id="url1")
    public WebElementFacade txtUrl;
    //Campo  email
    @FindBy(id="email1")
    public WebElementFacade txtemaill;
    //Campo Password1
    @FindBy(id="pass1")
    public WebElementFacade txtPass1;
    //Campo Password2
    @FindBy(id="pass2")
    public WebElementFacade txtPass2;
    //Campo MinSize
    @FindBy(id="minsize1")
    public WebElementFacade txtminsize1;
    //Campo MaxSize
    @FindBy(id="maxsize1")
    public WebElementFacade txtmaxsize1;
    //Campo Number
    @FindBy(id="number2")
    public WebElementFacade txtnumber2;
    //Campo IP
    @FindBy(id="ip")
    public WebElementFacade txtip;
    //Campo Date
    @FindBy(id="date3")
    public WebElementFacade txtdate3;
    //Campo DataEarLier
    @FindBy(id="past")
    public WebElementFacade txtpast;
    //Campo validra
    @FindBy(xpath="//div[14]//input[1]")
    public WebElementFacade btnvalidate;
    //Globo informativo
    @FindBy(xpath="//body/div/div/div/div/div/div/div/div/form/div[1]/div[1]/div[1]/div[1]")
    public WebElementFacade globoInformativo;

 public void Required(String datoPrueba){
    txtRequired.click();
    txtRequired.clear();
    txtRequired.sendKeys(datoPrueba);
 }
    public void Sport1(String datoPrueba){
        cmbSport1.click();
        cmbSport1.selectByVisibleText(datoPrueba);
    }
    public  void Multiple_select(String datoPrueba){
    cmbSport1.selectByVisibleText(datoPrueba);
    }
    public void Url(String datoPrueba){
        txtUrl.click();
        txtUrl.clear();
        txtUrl.sendKeys(datoPrueba);
    }
    public void emaill(String datoPrueba){
        txtemaill.click();
        txtemaill.clear();
        txtemaill.sendKeys(datoPrueba);
    }
    public void Pass1(String datoPrueba){
        txtPass1.click();
        txtPass1.clear();
        txtPass1.sendKeys(datoPrueba);
    }
    public void Pass2(String datoPrueba){
        txtPass2.click();
        txtPass2.clear();
        txtPass2.sendKeys(datoPrueba);
    }
    public void minsize1(String datoPrueba){
        txtminsize1.click();
        txtminsize1.clear();
        txtminsize1.sendKeys(datoPrueba);
    }
    public void maxsize1(String datoPrueba){
        txtmaxsize1.click();
        txtmaxsize1.clear();
        txtmaxsize1.sendKeys(datoPrueba);
    }
    public void number2(String datoPrueba){
        txtnumber2.click();
        txtnumber2.clear();
        txtnumber2.sendKeys(datoPrueba);
    }
    public void ip(String datoPrueba){
        txtip.click();
        txtip.clear();
        txtip.sendKeys(datoPrueba);
    }
    public void date3(String datoPrueba){
        txtdate3.click();
        txtdate3.clear();
        txtdate3.sendKeys(datoPrueba);
    }
    public void past(String datoPrueba){
        txtpast.click();
        txtpast.clear();
        txtpast.sendKeys(datoPrueba);
    }
    public void botonValidate() {
        btnvalidate.click();
    }
    public void form_sin_errores(){
    assertThat(globoInformativo.isCurrentlyVisible(), is(false));

}
    public void form_con_errores(){
        assertThat(globoInformativo.isCurrentlyVisible(), is(true));
    }

}