package com.choucair.formacion.pageobject;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.WebElement;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class ColorlibMenuPage  extends PageObject {

    //menu forms
    @FindBy(xpath = "//body/div/div[2]/ul[1]/li[6]/a[1]")
    public WebElement menuForms;
    //menu form general
    @FindBy(xpath = "//li[6]//ul[1]//li[1]//a[1]")
    public WebElement menuFormGeneral;
    //menu form Validation
    @FindBy(xpath = "//li[6]//ul[1]//li[2]//a[1]")
    public WebElement menuFormValidation;
    //Validacion mensaje
    @FindBy(xpath = "//body//div//div//div//div//div[1]//div[1]//div[1]//h5[1]")
    public WebElement lblFormularioValidation;

    public void menuFormValidation(){
        menuForms.click();
        menuFormValidation.click()  ;
        String srtMensaje = lblFormularioValidation.getText();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //assertThat(srtMensaje, containsString("Popup Validation"));

    }



}
