#language:es
  Característica: Busqueda en Dogpile

    este feature se trata de generar una busqueda o multiples busquedas en dogpile y validar
    los resulatdos de estas busquedas.


    Escenario:Buscar resultados relevantes
      Dado  Que Jose ingresa a dogpile
      Cuando Jose realiza una busqueda de: rock nacional
      Entonces Jose debe poder ver resulatdos que contengan la palabra: rock

    Esquema del escenario: Verificar que los resultado muestren texto relevante al titulo
      Dado  Que Jose ingresa a dogpile
      Cuando Jose realiza una busqueda de: rock nacional
      Entonces Jose  debe ver un texto: <texto> relevante con el titulo: <titulo>


      Ejemplos:
        | Descripcion         | titulo                           | texto                                       |
        | Resultado ingles    | Argentine rock - Wikipedia       | Argentine Rock Nacional is                  |
        | Resultado youtube   | rock nacional 80 90 00 - YouTube | Sign in to like videos,                     |
        | Resultado descargas | Descargar Discos Rock Nacional   | Si encontraste valioso alguno de los discos |

