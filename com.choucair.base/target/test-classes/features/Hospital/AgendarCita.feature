# Author : Juan.sanchezsist@gmail.com

@tag
Feature: Gestionar Cita Médica
 Como paciente
 Quiero realizar la solicitud de una cita médica
 A través del sistema de Administración de Hospitales

  @tag1
  Scenario Outline: Title of your scenario
    Given que Carlos necesita registrar un nuevo doctor
    When el realiza el registro del mismo en el aplicativo de Administración de Hospitales
      | nombre_completo  | apellidos | telefono  | tipo_documento  | documento  |
      | "Juan" | "Sanchez" | "3125127784" | "Cedula de ciudadania" | "80895766" |
   Then el verifica que se presente en pantalla el mensaje Datos guardados correctamente
   
   
  #@tag2
  #Scenario Outline: Title of your scenario outline
   # Given I want to write a step with <name>
    #When I check for the <value> in step
    #Then I verify the <status> in step

    #Examples: 
     # | nombre_completo  | apellidos | telefono  | tipo_documento  | documento  |
      # | "Juan" | "Sanchez" | "3125127784" | "Cedula de ciudadania" | "80895766" |
      