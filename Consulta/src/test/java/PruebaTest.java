import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.concurrent.TimeUnit;

public class PruebaTest {
    private WebDriver firefoxDriver;

    @Before
    public void abririDirver() {
        //Encontrar el archivo del geckodriver
        System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\geckodriver.exe");
        //creamos opciones sobre nuestro drivers
        FirefoxOptions options= new FirefoxOptions();
        options.setCapability( "marionette",  false);
        //nueva instancia de firefoxDriver
        firefoxDriver = new FirefoxDriver(options);
        //definimos tiempo de espera  de respuesta
        firefoxDriver.manage().timeouts().implicitlyWait( 10, TimeUnit.SECONDS);

    }

    @Test
    public void hacer_busqueda(){


        //ingresar a la pagina
        firefoxDriver.get("https://www.google.com/");

        //Ingresar texto en el buscador
        firefoxDriver.findElement(By.xpath("//input[@name='q']")).sendKeys("cantidad de paises de america 36 saber es practico");

        //presionar tecla enter
        firefoxDriver.findElement(By.xpath("//input[@name='q']")).sendKeys(Keys.ENTER);

        //hacer clic sobre el segundo enlace
        firefoxDriver.findElement(By.xpath("//div[@class='srg']//div[@class='g']//div//h3[@class='LC20lb DKV0Md'][contains(text(),'Países de América | Saber es práctico')]")).click();

        //Creamos un elemento web para poder hacer acciones mas complicadas
        firefoxDriver.findElement(By.xpath("//*[@id=\"post-7002\"]/div[1]/p[2]/span[1]/strong"));

        WebElement subtitulo = firefoxDriver.findElement(By.xpath("//span[contains(text(),'2020')]"));
        Assert.assertTrue(subtitulo.getText().contains("2020"));
    }
        @After
        public void cerrarDriver() {
            firefoxDriver.quit();
        }

}