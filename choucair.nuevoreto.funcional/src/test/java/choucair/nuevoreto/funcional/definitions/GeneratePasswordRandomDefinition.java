package choucair.nuevoreto.funcional.definitions;

import choucair.nuevoreto.funcional.steps.GeneratePasswordRandomSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class GeneratePasswordRandomDefinition {
	
	@Steps	
	GeneratePasswordRandomSteps generatePasswordRandomSteps;
		
	@Given("^enter the form$")
	public void enter_the_form() throws Throwable {
		generatePasswordRandomSteps.OpenTheUrl();
		
	}

	@When("^fill form$")
	public void fill_form() throws Throwable {
		generatePasswordRandomSteps.FillFormRandom();
	}
	
	@Then("^verify successful random case$")
	public void verify_successful_random_case() throws Throwable {
		generatePasswordRandomSteps.SuccessfulForm();
				
	}


}
