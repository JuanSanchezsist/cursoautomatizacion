package choucair.nuevoreto.funcional.definitions;

import choucair.nuevoreto.funcional.steps.RegistrationErrorSuccessfulSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class RegistrationErrorSuccessfulDefinition {
	
	@Steps
	RegistrationErrorSuccessfulSteps registrationErrorSuccessfulSteps;
	
	@Given("^That you need to register a account$")
	public void that_you_need_to_register_a_account() throws Throwable {
		registrationErrorSuccessfulSteps.OpenUrl();
	}

	@When("^Diligence the form with error$")
	public void diligence_the_form_with_error() throws Throwable {
		registrationErrorSuccessfulSteps.CorrectionForm();
	}

	@Then("^He verifies that the Opening message is displayed$")
	public void he_verifies_that_the_Opening_message_is_displayed()  {
		registrationErrorSuccessfulSteps.Verifyform();
	}

	/*@Given("^that you need to register a account$")
	public void that_you_need_to_register_a_account()  {
		registrationErrorSuccessfulSteps.OpenUrl();
				
	   
	}
	@When("^diligence the form with error$")
	public void diligence_the_form_with_error() throws Throwable {
		registrationErrorSuccessfulSteps.CorrectionForm();
		
	}
	@Then("^he verifies that the Opening message is displayed$")
	public void he_verifies_that_the_Opening_message_is_displayed() throws Throwable {
		registrationErrorSuccessfulSteps.Verifyform();
		
	   
	}*/


}
