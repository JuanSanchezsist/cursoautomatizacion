package choucair.nuevoreto.funcional.definitions;

import choucair.nuevoreto.funcional.steps.SuccessfulPasswordErrorSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class SuccessfulPasswordErrorDefinition {
	
	@Steps
	SuccessfulPasswordErrorSteps successfulPasswordErrorSteps;
		

@Given("^you need diligence the form$")
public void you_need_diligence_the_form()   {
	successfulPasswordErrorSteps.OpenUrlPage();
	}

@When("^diligence the form with wrong password$")
public void diligence_the_form_with_wrong_password()   {
	successfulPasswordErrorSteps.Wrongpassword();
	   
}

@Then("^Verify that the success password is displayed$")
public void verify_that_the_success_password_is_displayed()  {
	successfulPasswordErrorSteps.successfulCase();
	
    
}
	


}
