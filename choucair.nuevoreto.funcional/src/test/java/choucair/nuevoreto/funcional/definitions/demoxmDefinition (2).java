package choucair.nuevoreto.funcional.definitions;

import choucair.nuevoreto.funcional.steps.demoxmSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class demoxmDefinition {
	
	@Steps
	demoxmSteps DemoxmSteps;
		
	
	@Given("^that you need to register a demo account$")
	public void that_you_need_to_register_a_demo_account() throws Throwable {
		DemoxmSteps.OpenUrlXm();
	
	}

	@When("^diligence the form$")
	public void diligence_the_form()  {
		DemoxmSteps.FillForm();
	  
	}

	@Then("^he verifies that the opening message is displayed on the screen$")
	public void he_verifies_that_the_opening_message_is_displayed_on_the_screen() throws Throwable {
		DemoxmSteps.VerifyAccount();
	}

}
