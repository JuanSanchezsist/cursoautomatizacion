package choucair.nuevoreto.funcional.steps;

import choucair.nuevoreto.funcional.pageobjects.RegistrationErrorSuccessfulPageObjects;
import net.thucydides.core.annotations.Step;

public class RegistrationErrorSuccessfulSteps {
	
	RegistrationErrorSuccessfulPageObjects registrationErrorSuccessfulPageObjects;
	
	@Step
	public void OpenUrl() {
		registrationErrorSuccessfulPageObjects.open();
				
	}
	@Step
	public void CorrectionForm() {
		
		String firstname = "Ñ0ñ0";
		String firstnamedos = "pedro";
		String lastname = "Perez";
		String city = "Bogota";
		String phonenumber = "31351275698";
		String email = "prueba2@chouca.com";
		String password = "pxQQpP09";
		String passwordconfirmation = "pxQQpP09";
		
		registrationErrorSuccessfulPageObjects.CorrectionForm(firstname, firstnamedos, lastname, city, phonenumber, email, password, passwordconfirmation);
	
		
	}
	@Step
	public void Verifyform() {
		registrationErrorSuccessfulPageObjects.Verifyform();		
	}

}
