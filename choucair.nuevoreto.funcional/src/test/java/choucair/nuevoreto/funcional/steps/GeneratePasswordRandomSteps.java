package choucair.nuevoreto.funcional.steps;

import org.apache.commons.lang3.RandomStringUtils;
import choucair.nuevoreto.funcional.pageobjects.GeneratePasswordRandomPageObjects;
import net.thucydides.core.annotations.Step;

public class GeneratePasswordRandomSteps {
	
	GeneratePasswordRandomPageObjects generatePasswordRandomPageObjects;
	
	@Step
	public void OpenTheUrl() {
	 generatePasswordRandomPageObjects.open();
				
	}
   @Step
	public void FillFormRandom() {
	
	String firstname = "Jose";
	String lastname = "Sanchez";
	String city = "Bogota";
	String phonenumber = "31351275698";
	String email = "prueba2@chouca.com";
	generatePasswordRandomPageObjects.fillFormRandom(firstname, lastname, city, phonenumber, email);
	
	}
   @Step
	public void SuccessfulForm() {

	   String generatedString = RandomStringUtils.randomAlphanumeric(10);
	   String generatedStringdos = RandomStringUtils.randomAlphanumeric(10);

	   generatePasswordRandomPageObjects.SuccessfulForm(generatedString, generatedStringdos);
	   
		
	}

}
