package choucair.nuevoreto.funcional.steps;

import choucair.nuevoreto.funcional.pageobjects.demoxmPageObjects;
import net.thucydides.core.annotations.Step;

public class demoxmSteps {

	demoxmPageObjects DemoxmPageObjects;

	@Step
	public void OpenUrlXm() {

		DemoxmPageObjects.open();
		DemoxmPageObjects.click();

	}

	@Step

	public void FillForm() {

		String firstname = "Juan";
		String lastname = "Sanchez";
		String city = "Bogota";
		String phonenumber = "31351275698";
		String email = "prueba@chouca.com";
		String password = "pxQQpP09";
		String passwordconfirmation = "pxQQpP09";

		DemoxmPageObjects.FillForm(firstname, lastname, city, phonenumber, email, password, passwordconfirmation);

	}

	@Step
	public void VerifyAccount() {
		DemoxmPageObjects.VerifyAccount();
	}

	


}
