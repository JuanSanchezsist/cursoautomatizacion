package choucair.nuevoreto.funcional.steps;

import choucair.nuevoreto.funcional.pageobjects.SuccessfulPasswordErrorPageObjects;
import net.thucydides.core.annotations.Step;

public class SuccessfulPasswordErrorSteps {
	
	SuccessfulPasswordErrorPageObjects successfulPasswordErrorPageObjects;
	
	
	@Step
	public void OpenUrlPage() {
	successfulPasswordErrorPageObjects.open();
					
	}
	@Step
	public void Wrongpassword() {
		
		String firstname = "Jose";
		String lastname = "Sanchez";
		String city = "Bogota";
		String phonenumber = "31351275698";
		String email = "prueba2@chouca.com";
				
		successfulPasswordErrorPageObjects.wrongpassword(firstname, lastname, city, phonenumber, email);
				
	}
@Step
	public void successfulCase() {
				String password = "pxQQpP09";
		String passwordconfirmation = "zz356pxQQpP09";
		String passwordconfirmationDos = "";
		successfulPasswordErrorPageObjects.SuccessfulCase(password, passwordconfirmation, passwordconfirmationDos);
		
	}

}
