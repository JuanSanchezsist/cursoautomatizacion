package choucair.nuevoreto.funcional.steps;

import choucair.nuevoreto.funcional.pageobjects.demoxmPageObjects;
import net.thucydides.core.annotations.Step;

public class demoxmSteps {

	demoxmPageObjects DemoxmPageObjects;

	@Step
	public void OpenUrlXm() {

		DemoxmPageObjects.open();
		DemoxmPageObjects.click();

	}

	@Step

	public void FillForm() {

		String firstname = "carlos";
		String lastname = "Contrera";
		String city = "Bogota";
		String phonenumber = "31351265698";
		String email = "prueba@choucas.com";
		String password = "pxQQpP09u";
		String passwordconfirmation = "pxQQpP09u";

		DemoxmPageObjects.FillForm(firstname, lastname, city, phonenumber, email, password, passwordconfirmation);

	}

	@Step
	public void VerifyAccount() {
		DemoxmPageObjects.VerifyAccount();
	}

	


}
