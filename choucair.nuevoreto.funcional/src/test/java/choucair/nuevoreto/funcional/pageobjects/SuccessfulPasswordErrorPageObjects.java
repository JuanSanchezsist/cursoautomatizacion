package choucair.nuevoreto.funcional.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.xm.com/register/account/demo?lang=es")

public class SuccessfulPasswordErrorPageObjects extends PageObject {
	
	@FindBy(xpath = "//*[@id=\"first_name\"]")
	public WebElementFacade txtfirstname;
	@FindBy(xpath = "//*[@id='demo-form']/div[3]/div[2]/div[1]/div/label[2]")
	public WebElementFacade Errormessage;
	@FindBy(xpath = "//*[@id=\"first_name\"]")
	public WebElementFacade txtfirstnamedos;
	@FindBy(xpath = "//*[@id='last_name']")
	public WebElementFacade txtlastname;
	@FindBy(xpath = "//*[@id=\'country\']")
	public WebElementFacade dropcountry;
	@FindBy(xpath = "//*[@id=\'city\']")
	public WebElementFacade txtcity;
	@FindBy(xpath = "//*[@id=\'phone_number\']")
	public WebElementFacade txtphonenumber;
	@FindBy(xpath = "//*[@id=\'email\']")
	public WebElementFacade txtemail;
	@FindBy(xpath = "//*[@id=\'preferred_language\']/option[16]")
	public WebElementFacade droplanguage;
	@FindBy(xpath = "//*[@id='account_password']")
	public WebElementFacade txtpassword;
	@FindBy(xpath = "//*[@id=\'account_password_confirmation\']")
	public WebElementFacade txtpasswordconfirmation;
	@FindBy(xpath = "//*[@id='demo-form']/div[3]/div[14]/div/div/div/label")
	public WebElementFacade chckbxacceptnews;
	@FindBy(xpath = "//*[@id='submit-btn']")
	public WebElementFacade btnopenaccount;
	@FindBy(xpath = "//*[@id='top']/div[1]/div/div/div[1]/div[1]/div/div[2]/div/div/div[2]/h2")
	public WebElementFacade verifiedcreated;
	@FindBy(xpath = "//*[@id=\'cookieModal\']/div/div/div[1]/div[2]/div[2]/div/button")
	public WebElementFacade btncookieModal;
	 @FindBy(xpath ="//*[@id=\'demo-form\']/div[3]/div[13]/div[3]/div/label[2]")
	 public WebElementFacade msjdemoformUno;
	 @FindBy(xpath ="//*[@id=\'demo-form\']/div[3]/div[13]/div[3]/div/label[2]/strong")
	 public WebElementFacade msjdemoformDos;
	 @FindBy(xpath = "//*[@id='demo-form']/div[3]/div[14]")
	 public WebElementFacade ClickFuera;
	
	public void wrongpassword(String firstname, String lastname, String city, String phonenumber, String email) {
		findBy("//*[@id=\'cookieModal\']/div/div/div[1]/div[2]/div[2]/div/button").click();
		txtfirstname.click();
		txtfirstname.sendKeys(firstname);
		txtlastname.sendKeys(lastname);
		txtcity.sendKeys(city);
		txtphonenumber.sendKeys(phonenumber);
		txtemail.sendKeys(email);
		droplanguage.click();
			
	}

	public void SuccessfulCase(String password, String passwordconfirmation, String passwordconfirmationDos) {
		
		try { 
			 
			txtpassword.sendKeys(password);
			txtpasswordconfirmation.sendKeys(passwordconfirmation); 
			ClickFuera.click();
			
		if ( msjdemoformUno.isVisible ()) {
			txtpasswordconfirmation.clear();
			txtpasswordconfirmation.sendKeys(passwordconfirmationDos);
		}

		} catch (Exception e) {
			txtpasswordconfirmation.sendKeys(passwordconfirmationDos);
				System.out.println("error las claves son iguales:" + e);
		
		}
		assertThat(msjdemoformUno.isCurrentlyVisible(), is(true));
		assertThat(msjdemoformDos.isCurrentlyVisible(), is(true));
		
	}
	
	

}
