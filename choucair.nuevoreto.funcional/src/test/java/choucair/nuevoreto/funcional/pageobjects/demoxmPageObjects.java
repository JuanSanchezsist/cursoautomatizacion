package choucair.nuevoreto.funcional.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("https://www.xm.com/register/account/demo?lang=es")

public class demoxmPageObjects extends PageObject {

	@FindBy(xpath = "//*[@id=\"first_name\"]")
	public WebElementFacade txtfirstname;
	@FindBy(xpath = "//*[@id='last_name']")
	public WebElementFacade txtlastname;
	@FindBy(xpath = "//*[@id=\'country\']")
	public WebElementFacade dropcountry;
	@FindBy(xpath = "//*[@id=\'city\']")
	public WebElementFacade txtcity;
	@FindBy(xpath = "//*[@id=\'phone_number\']")
	public WebElementFacade txtphonenumber;
	@FindBy(xpath = "//*[@id=\'email\']")
	public WebElementFacade txtemail;
	@FindBy(xpath = "//*[@id=\'preferred_language\']/option[16]")
	public WebElementFacade droplanguage;
	@FindBy(xpath = "//*[@id='account_password']")
	public WebElementFacade txtpassword;
	@FindBy(xpath = "//*[@id=\'account_password_confirmation\']")
	public WebElementFacade txtpasswordconfirmation;
	@FindBy(xpath = "//*[@id='demo-form']/div[3]/div[14]/div/div/div/label")
	public WebElementFacade chckbxacceptnews;
	@FindBy(xpath = "//*[@id='submit-btn']")
	public WebElementFacade btnopenaccount;
	@FindBy(xpath = "//*[@id='top']/div[1]/div/div/div[1]/div[1]/div/div[2]/div/div/div[2]/h2")
	public WebElementFacade verifiedcreated;

	public void click() {
		findBy("//*[@id=\'cookieModal\']/div/div/div[1]/div[2]/div[2]/div/button").click();
		txtfirstname.click();
	}
	
	public void FillForm(String firstname, String lastname, String city, String phonenumber, String email,String password, String passwordconfirmation) {
				
		try {

			txtfirstname.sendKeys(firstname);
			txtlastname.sendKeys(lastname);
			txtcity.sendKeys(city);
			txtphonenumber.sendKeys(phonenumber);
			txtemail.sendKeys(email);
			droplanguage.click();
			txtpassword.sendKeys(password);
			txtpasswordconfirmation.sendKeys(passwordconfirmation);
			chckbxacceptnews.click();
			btnopenaccount.click();

		} catch (Exception e) {
			System.out.println("error diligencias formulario" + e);
		}
		
	}

	public void VerifyAccount() {
		String mensaje = verifiedcreated.getText();
		assertThat(mensaje, containsString("Enhorabuena por la apertura de una Cuenta Demo de XM"));
	}

	

}

