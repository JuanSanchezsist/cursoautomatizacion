package choucair.nuevoreto.funcional.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.xm.com/register/account/demo?lang=es")

public class GeneratePasswordRandomPageObjects extends PageObject {
	
	@FindBy(xpath = "//*[@id=\"first_name\"]")
	public WebElementFacade txtfirstname;
	@FindBy(xpath = "//*[@id='demo-form']/div[3]/div[2]/div[1]/div/label[2]")
	public WebElementFacade Errormessage;
	@FindBy(xpath = "//*[@id=\"first_name\"]")
	public WebElementFacade txtfirstnamedos;
	@FindBy(xpath = "//*[@id='last_name']")
	public WebElementFacade txtlastname;
	@FindBy(xpath = "//*[@id=\'country\']")
	public WebElementFacade dropcountry;
	@FindBy(xpath = "//*[@id=\'city\']")
	public WebElementFacade txtcity;
	@FindBy(xpath = "//*[@id=\'phone_number\']")
	public WebElementFacade txtphonenumber;
	@FindBy(xpath = "//*[@id=\'email\']")
	public WebElementFacade txtemail;
	@FindBy(xpath = "//*[@id=\'preferred_language\']/option[16]")
	public WebElementFacade droplanguage;
	@FindBy(xpath = "//*[@id='account_password']")
	public WebElementFacade txtpassword;
	@FindBy(xpath = "//*[@id=\'account_password_confirmation\']")
	public WebElementFacade txtpasswordconfirmation;
	 @FindBy(xpath ="//*[@id=\'demo-form\']/div[3]/div[13]/div[3]/div/label[2]")
	 public WebElementFacade msjdemoformUno;
	@FindBy(xpath = "//*[@id='demo-form']/div[3]/div[14]")
	public WebElementFacade ClickFuera;

	public void fillFormRandom(String firstname, String lastname, String city, String phonenumber, String email) {
		
		findBy("//*[@id=\'cookieModal\']/div/div/div[1]/div[2]/div[2]/div/button").click();
		txtfirstname.click();
		txtfirstname.sendKeys(firstname);
		txtlastname.sendKeys(lastname);
		txtcity.sendKeys(city);
		txtphonenumber.sendKeys(phonenumber);
		txtemail.sendKeys(email);
		droplanguage.click();
		}

	public void SuccessfulForm(String generatedString) {
	
	}

	public void SuccessfulForm(String generatedString, String generatedStringdos) {
		txtpassword.sendKeys(generatedString);
		txtpasswordconfirmation.sendKeys(generatedStringdos); 
		ClickFuera.click();
		assertThat(msjdemoformUno.isCurrentlyVisible(), is(true));
		System.out.println ("El password ingresado es:"+ generatedString  +" la confirmacion de la contraseña es:"+ generatedStringdos );
				
	}

}
