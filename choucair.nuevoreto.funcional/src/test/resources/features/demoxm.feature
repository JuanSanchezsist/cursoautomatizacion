#Author: juan.sanchezsist@gmail.com

@ScenarioFull
	Feature: Register demo account
  Create a demo account of different forms
  
@SuccessfulCases
	  Scenario: Successful registration
    Given that you need to register a demo account
    When diligence the form
    Then he verifies that the opening message is displayed on the screen
 @SuccessfulCases
 		Scenario: Successful registration with correction
 		 Given That you need to register a account
 		 When  Diligence the form with error
 		 Then  He verifies that the Opening message is displayed

@ErrorOrientedCases
		
 		 Scenario: Password error
 		 	Given you need diligence the form
 		 	When diligence the form with wrong password
 		 	Then Verify that the success password is displayed
@ErrorOrientedCases
  		Scenario: Password error Random
   		 	Given enter the form
    		When fill form
    		Then verify successful random case
    
 		 