#Author: juan.sanchezsist@gmail.com

@tag
Feature: anage Medical Appointment
  I want to use this template for my feature file

  @AddDoctor
  Scenario: Title of your scenario
    Given that Carlos needs to register a new doctor
    When he registers it in the Hospital Administration application 
    And enter the data Full name "Juan" Surnames "Sanchez" telephone number "3122567890" Type identity document  "Cedula_de_Ciudadania" identification document "88895656"
    Then Verifies that the message Data saved correctly appears on the screen
   
    @AddPatient
    Scenario Outline: Perform a Patient Registration
    Given that Carlos needs to register a new patient
    When  it is registered in the Hospital Administration application
    And enter the data Full name <name> Surnames <surname> telephone number <telephone> Type identity document <Typeidentity>  identification document <identification> 
    Then  you will see that the message Data saved correctly is present on the screen
         
    Examples: 
     | name      | surname    | telephone    | Typeidentity              | identification | 
     | "juan"    | "sanchez"  | "3125127783" | "cedula de ciudadania" 	 | " 1023654787"  | 
     | "milena"  | "pineda	" | "311265489"  | "cedula de ciudadania"    | "1023632166"   |
     | "Jair"   | "Perez	"   | "311265459"  | "cedula de ciudadania"    | "1033632177"   |
      
   @AddCita
			Scenario: Make the Appointment Scheduling
		  Given that	carlos needs to assist the doctor
			When sacheduling an Appointment
      | dayappointment | identityPatient | doctoridentity | observations       | 
      | 19/03/2019     | 1023632166      | 88895656 	    |  cita programada   | 
			Then you will see that message Data saved correctly is present on the screen
      
     
      
      