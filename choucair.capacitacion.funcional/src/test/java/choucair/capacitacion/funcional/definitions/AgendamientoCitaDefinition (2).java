package choucair.capacitacion.funcional.definitions;

import cucumber.api.java.en.Given;
import choucair.capacitacion.funcional.steps.AgendamientoCitaSteps;
import choucair.capacitacion.funcional.steps.GestionarCitaSteps;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;


public class AgendamientoCitaDefinition {
	
	@Steps
	AgendamientoCitaSteps agendamientoCitaSteps;
	
	
	@Given("^that Carlos needs to register a new patient$")
	public void that_Carlos_needs_to_register_a_new_patient() throws Throwable {
		agendamientoCitaSteps.AbrirUrlHospital();
		
	}
		
	@When("^it is registered in the Hospital Administration application$")
	public void it_is_registered_in_the_Hospital_Administration_application() throws Throwable {
		agendamientoCitaSteps.AgendarPaciente();
	}
	
	

   @When("^enter the data Full name \"([^\"]*)\" Surnames \"([^\"]*)\" telephone number \"([^\"]*)\" Type identity document \"([^\"]*)\"  identification document \"([^\"]*)\"$")
    public void enter_the_data_Full_name_Surnames_telephone_number_Type_identity_document_identification_document(String name, String surname, String telephone, String Typeidentity, String identification) throws Throwable {
	   agendamientoCitaSteps.IngresarPaciente(name, surname, telephone, Typeidentity, identification); 
    }
   @Then("^you will see that the message Data saved correctly is present on the screen$")
           public void you_will_see_that_the_message_Data_saved_correctly_is_present_on_the_screen() throws Throwable {
	           agendamientoCitaSteps.VerificarRegistro();
    }
}
