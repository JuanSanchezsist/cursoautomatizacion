package choucair.capacitacion.funcional.definitions;

import choucair.capacitacion.funcional.steps.IngresoLoginPlanetSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class LoginEncuentrameDefinition {
	

@Steps 
	IngresoLoginPlanetSteps ingresoLoginPlanetSteps;


	
	@Given("^que tengo mis datos de ingreso$")
	public void que_tengo_mis_datos_de_ingreso() throws Throwable {
		ingresoLoginPlanetSteps.AbrirUrlPlanet();
		
	    
	}

	@When("^ingreso el usuario \"([^\"]*)\"$")
	public void ingreso_el_usuario(String usuario) throws Throwable {
		ingresoLoginPlanetSteps.ingresoUsuario(usuario);
	}

	@When("^ingreso al contraseña \"([^\"]*)\"$")
	public void ingreso_al_contraseña(String clave) throws Throwable {
		ingresoLoginPlanetSteps.ingresoClave(clave);
		
	}

	@Then("^Verifico que ingrese al aplicactivo$")
	public void verifico_que_ingrese_al_aplicactivo() throws Throwable {
		ingresoLoginPlanetSteps.verificarIngreso();
		
	}

	@When("^ingreso de ususario <txNombre>$")
	public void ingreso_de_ususario_txNombre() throws Throwable {
	    
	}

	@When("^ingrese la contraseña <txtClave>$")
	public void ingrese_la_contraseña_txtClave() throws Throwable {
	   
	}

	@Then("^verifico que ingrese al aplicactivo$")
	public void verifico_que_ingrese_al_aplicactivo() throws Throwable {
	   

	}
}
