package choucair.capacitacion.funcional.definitions;

import choucair.capacitacion.funcional.steps.GestionarCitaSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class GestionarCitasDefinition {

	@Steps
	GestionarCitaSteps gestionarCitasSteps;
	
	

@Given("^that Carlos needs to register a new doctor$")
public void that_Carlos_needs_to_register_a_new_doctor() throws Throwable {
	gestionarCitasSteps.AbrirUrlHospital();
	   
}

@When("^he registers it in the Hospital Administration application$")
public void he_registers_it_in_the_Hospital_Administration_application() throws Throwable {
	gestionarCitasSteps.AgregarDoctor();
	
  
}

@When("^enter the data Full name \"([^\"]*)\" Surnames \"([^\"]*)\" telephone number \"([^\"]*)\" Type identity document  \"([^\"]*)\" identification document \"([^\"]*)\"$")
public void enter_the_data_Full_name_Surnames_telephone_number_Type_identity_document_identification_document(String name, String Surnames, String telephonenumber, String Typeidentitydocument, String identificationdocument) throws Throwable {
	gestionarCitasSteps.IngresarDatos(name, Surnames, telephonenumber, Typeidentitydocument, identificationdocument );
}

@Then("^Verifies that the message Data saved correctly appears on the screen$")
public void verifies_that_the_message_Data_saved_correctly_appears_on_the_screen() throws Throwable {
	gestionarCitasSteps.VerificarIngreso();
	   
}

@Given("^I want to write a step with name(\\d+)$")
public void i_want_to_write_a_step_with_name(int arg1) throws Throwable {
    
}

@When("^I check for the (\\d+) in step$")
public void i_check_for_the_in_step(int arg1) throws Throwable {

}

@Then("^I verify the success in step$")
public void i_verify_the_success_in_step() throws Throwable {
  
}

@Then("^I verify the Fail in step$")
public void i_verify_the_Fail_in_step() throws Throwable {
  
}

}

