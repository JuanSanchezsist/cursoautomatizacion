package choucair.capacitacion.funcional.definitions;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.List;

import choucair.capacitacion.funcional.steps.ProgramarcitaSteps;
import net.thucydides.core.annotations.Steps;

public class ProgramarcitaDefinition {
	
	@Steps
	ProgramarcitaSteps programarcitaSteps;
	

	@Given("^that	carlos needs to assist the doctor$")
	public void that_carlos_needs_to_assist_the_doctor(){
	programarcitaSteps.AbrirUrlCita();
		
	}

	@When("^sacheduling an Appointment$")
	public void scheduling_an_Appointment(DataTable dtDatosForm) throws InterruptedException {
		List<List<String>> data = dtDatosForm.raw();
		Thread.sleep(5000);
		for (int i = 1; i < data.size(); i++) {
			programarcitaSteps.SeleccionarDatos(data, i);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
			}
		}
	}

	@Then("^you will see that message Data saved correctly is present on the screen$")
	public void you_will_see_that_message_Data_saved_correctly_is_present_on_the_screen() {
	programarcitaSteps.VerificarCita();
				

	}
}
