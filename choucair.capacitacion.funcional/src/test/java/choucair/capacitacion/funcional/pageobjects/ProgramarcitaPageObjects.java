package choucair.capacitacion.funcional.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

import org.openqa.selenium.Keys;

@DefaultUrl("http://automatizacion.herokuapp.com/pperez/")

public class ProgramarcitaPageObjects extends PageObject {

	@FindBy(xpath = "//*[@id=\'datepicker\']")
	public WebElementFacade txtdatepicker;
	@FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[3]/div/div[2]/input")
	public WebElementFacade txtdocpaciente;
	@FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[3]/div/div[3]/input")
	public WebElementFacade txtdocdoctor;
	@FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[3]/div/div[4]/textarea")
	public WebElementFacade txttarea;
	@FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[3]/div/a")
	public WebElementFacade btnguardar;
	@FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[2]/div[1]/h3")
	public WebElementFacade lblguardado;

	public void AgendarCita() {
		findBy("//*[@id=\'page-wrapper\']/div/div[2]/div/div/div/div/div[1]/div/a[6]").click();
	}

	public void dayappointment(String dataUser) {
		txtdatepicker.sendKeys(dataUser);
		txtdatepicker.sendKeys(Keys.ENTER);

	}

	public void identityPatient(String dataUser) {
		txtdocpaciente.click();
		txtdocpaciente.clear();
		txtdocpaciente.sendKeys(dataUser);

	}

	public void doctoridentity(String dataUser) {
		txtdocdoctor.click();
		txtdocdoctor.clear();
		txtdocdoctor.sendKeys(dataUser);

	}

	public void observations(String dataUser) {
		txttarea.click();
		txttarea.clear();
		txttarea.sendKeys(dataUser);

	}

	public void VerificarCita() {
		btnguardar.click();
		lblguardado.click();
		String mensaje = lblguardado.getText();
		assertThat(mensaje, containsString("Guardado:"));

	}

}
