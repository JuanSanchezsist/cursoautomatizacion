package choucair.capacitacion.funcional.pageobjects;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import org.openqa.selenium.WebElement;

@DefaultUrl("http://automatizacion.herokuapp.com/pperez/")

public class GestionarCitaPageObjects extends PageObject {
	
	@FindBy(xpath= "//*[@id=\'page-wrapper\']/div/div[2]/div[1]/h3")
	public WebElement lblValidation;
	

	public void AgregarDoctor() {
		findBy("//*[@id=\'page-wrapper\']/div/div[2]/div/div/div/div/div[1]/div/a[1]").click();
		
	}

	public void IngresarDatos(String name, String surnames, String telephonenumber, String typeidentitydocument,
			String identificationdocument) {
			findBy("//*[@id='name']").sendKeys(name);
			findBy("//*[@id=\'last_name\']").sendKeys(surnames);
			findBy("//*[@id=\'telephone\']").sendKeys(telephonenumber);
			findBy("//*[@id=\'identification_type\']").sendKeys(typeidentitydocument);
			findBy("//*[@id=\'identification\']").sendKeys(identificationdocument);		
	}

	public void VerificarIngreso() {
		findBy("//*[@id=\'page-wrapper\']/div/div[3]/div/a").click();
		findBy("//*[@id=\'page-wrapper\']/div/div[2]/div[1]/h3").click();
		String mensaje = lblValidation.getText();
		assertThat(mensaje, containsString("Guardado:"));
		
		
		
	}

}
