package choucair.capacitacion.funcional.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import org.openqa.selenium.Keys;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://automatizacion.herokuapp.com/pperez/")

public class AgendamientoCitaPageObjects extends PageObject {
	
	@FindBy(xpath= "//*[@id=\'page-wrapper\']/div/div[3]/div/div[1]/input")
	public WebElementFacade txtname;
	@FindBy(xpath= "//*[@id=\'page-wrapper\']/div/div[3]/div/div[2]/input")
	public WebElementFacade txtlastname;
	@FindBy(xpath= "//*[@id=\'page-wrapper\']/div/div[3]/div/div[3]/input")
	public WebElementFacade txttelephone;
	@FindBy(xpath= "//*[@id=\'page-wrapper\']/div/div[3]/div/div[4]/select/option[1] ")
	public WebElementFacade cmbidentification_type;
	@FindBy(xpath= "//*[@id=\'page-wrapper\']/div/div[3]/div/div[5]/input")
	public WebElementFacade txtidentification;
	@FindBy(xpath= "//*[@id=\'page-wrapper\']/div/div[3]/div/div[6]/label/input")
	public WebElementFacade cbxprepaid;
	@FindBy(xpath= "//*[@id=\'page-wrapper\']/div/div[3]/div/a")
	public WebElementFacade btnguardar;
	@FindBy(xpath= "//*[@id=\'page-wrapper\']/div/div[2]/div[1]/h3")
	public WebElementFacade lblguardado;

	public void AgendarPaciente() {
		findBy("//*[@id=\'page-wrapper\']/div/div[2]/div/div/div/div/div[1]/div/a[2]").click();
		
	}

	public void IngresarPaciente(String name, String surname, String telephone, String typeidentity,
			String identification) {
			txtname.click();
			txtname.sendKeys(name);
			txtlastname.sendKeys(surname);
			txttelephone.sendKeys(telephone);
			cmbidentification_type.click();
			txtidentification.sendKeys(identification);
			cbxprepaid.click();
						
	}

	public void VerificarRegistro() {
		btnguardar.click();
		lblguardado.click();
		String mensaje = lblguardado.getText();
		assertThat(mensaje, containsString("Guardado:"));
		
		
	}
	

}
