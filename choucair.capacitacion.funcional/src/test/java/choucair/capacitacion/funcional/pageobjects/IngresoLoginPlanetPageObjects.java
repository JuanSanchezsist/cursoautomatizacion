package choucair.capacitacion.funcional.pageobjects;


import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl ("http://planet.choucairtesting.com/bin/login/Main/WebHome?origurl=/")

public class IngresoLoginPlanetPageObjects extends PageObject {

	public void IngresoUsuario(String usuario) {
		find(By.name("username")).type(usuario);
				
	}
	public void IngresoClave(String clave) {
		find(By.name("password")).typeAndEnter(clave);
		
		
	}

	public void VerificarIngreso() {
		findBy("//*[@id=\'patternSideBarContents'\"]/span[1]/a").click();
				
	}

	
	
	
	
	

}
