package choucair.capacitacion.funcional.steps;

import choucair.capacitacion.funcional.pageobjects.AgendamientoCitaPageObjects;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import net.thucydides.core.annotations.Step;


public class AgendamientoCitaSteps {
	
	AgendamientoCitaPageObjects agendamientoCitaPageObjects;
	

	public void AbrirUrlHospital() {
		agendamientoCitaPageObjects.open();
		
		
	}

	public void AgendarPaciente() {
		agendamientoCitaPageObjects.AgendarPaciente();
		
	}

	public void IngresarPaciente(String name, String surname, String telephone, String typeidentity,
			String identification) {
		agendamientoCitaPageObjects.IngresarPaciente(name, surname, telephone, typeidentity, identification);
	}

	public void VerificarRegistro() {
		agendamientoCitaPageObjects.VerificarRegistro();
				
	}

}
