package choucair.capacitacion.funcional.steps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import choucair.capacitacion.funcional.pageobjects.GestionarCitaPageObjects;
import net.thucydides.core.annotations.Step;


public class GestionarCitaSteps {
	
	GestionarCitaPageObjects gestionarCitaPageObjects;
	
	@Step
	public void AbrirUrlHospital() {
		gestionarCitaPageObjects.open();
		
	}
	@Step
	public void AgregarDoctor() {
		gestionarCitaPageObjects.AgregarDoctor();
		
	}
	@Step
	public void IngresarDatos(String name, String surnames, String telephonenumber, String typeidentitydocument,
			String identificationdocument) {
		gestionarCitaPageObjects.IngresarDatos(name, surnames, telephonenumber,  typeidentitydocument, identificationdocument);
				
	}
	@Step
	public void VerificarIngreso() {
		gestionarCitaPageObjects.VerificarIngreso();
		
	}

}
