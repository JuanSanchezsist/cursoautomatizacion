package choucair.capacitacion.funcional.steps;

import java.util.List;

import choucair.capacitacion.funcional.pageobjects.ProgramarcitaPageObjects;
import net.thucydides.core.annotations.Step;

public class ProgramarcitaSteps {
	
	 ProgramarcitaPageObjects programarcitaPageObjects;
	 
	@Step
	public void AbrirUrlCita() {
		programarcitaPageObjects.open();
		programarcitaPageObjects.AgendarCita();
		
   }
	@Step
	public void SeleccionarDatos(List<List<String>>data, int id) {
		programarcitaPageObjects.dayappointment(data.get(id).get(0).trim());
		programarcitaPageObjects.identityPatient(data.get(id).get(1).trim());
		programarcitaPageObjects.doctoridentity(data.get(id).get(2).trim());
		programarcitaPageObjects.observations(data.get(id).get(3).trim());
	}
		
@Step
	public void VerificarCita() {
		programarcitaPageObjects.VerificarCita();
		
	}

}
