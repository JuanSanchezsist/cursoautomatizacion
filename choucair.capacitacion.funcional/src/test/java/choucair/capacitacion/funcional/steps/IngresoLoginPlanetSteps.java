package choucair.capacitacion.funcional.steps;

import choucair.capacitacion.funcional.pageobjects.IngresoLoginPlanetPageObjects;
import net.thucydides.core.annotations.Step;




public class IngresoLoginPlanetSteps {
	
	IngresoLoginPlanetPageObjects IngresoLoginPlanetPageObjects;
	
@Step
	public void AbrirUrlPlanet() {
	IngresoLoginPlanetPageObjects.open();
		
	}
@Step
	public void ingresoUsuario(String usuario) {
	IngresoLoginPlanetPageObjects.IngresoUsuario(usuario);
		
	}
@Step
	public void ingresoClave(String clave) {
	IngresoLoginPlanetPageObjects.IngresoClave(clave);
		
	}
@Step
	public void verificarIngreso() {
		IngresoLoginPlanetPageObjects.VerificarIngreso();
		}

}
