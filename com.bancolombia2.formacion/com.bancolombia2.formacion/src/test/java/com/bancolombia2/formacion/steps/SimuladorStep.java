package com.bancolombia2.formacion.steps;

import com.bancolombia2.formacion.pageobjects.SimuladorPage;

import net.thucydides.core.annotations.Step;

public class SimuladorStep {
	
	 SimuladorPage simuladorPage;

	    @Step
	    public void ingresar_pagina_simulacro() {
	        simuladorPage.open();
	    }
	    @Step
	    public void  diligenciar_formulario( ){
	        simuladorPage.simulaCuota("Simula tu Cuota");
	        simuladorPage.ingresar_fecha_nacimiento();
	        simuladorPage.ingresa_tipo_tasa("Tasa Fija");
	        simuladorPage.ingresa_tipo_producto("Crédito de Libre Inversión");
	        //simuladorPage.scrollpagina();
	        simuladorPage.checkbox();
	        simuladorPage.plazo_prestamo();
	        simuladorPage.valor_deseado();
	        simuladorPage.ejeuctar_simulacion();
	        
	        

	    }    
	    public void validarresult(int dato) {
	    	simuladorPage.resultado(10000000);
	    	
	    }

}
