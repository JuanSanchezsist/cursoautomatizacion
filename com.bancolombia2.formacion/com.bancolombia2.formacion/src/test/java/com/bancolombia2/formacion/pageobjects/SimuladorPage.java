package com.bancolombia2.formacion.pageobjects;


import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import java.lang.Math; 
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@DefaultUrl("https://www.grupobancolombia.com/wps/portal/personas/productos-servicios/creditos/consumo/libre-inversion/simulador-credito-consumo")
	public class SimuladorPage extends PageObject {
	 
	 
	 
	    @FindBy(xpath = "//*[@id=\'sim-detail\']/form/div[2]/select")
	    public WebElementFacade dropSimularCuota;
	    
	    @FindBy(xpath = "//*[@id=\'sim-detail\']/form/div[2]/p[1]")
	    public  WebElement mensajesimulador;

	    @FindBy(xpath = "//form[@name='creditoconsumoForm']/descendant::input[@name='dateFechaNacimiento']")
	    public  WebElement cajaFechaNacimiento;

	    @FindBy(xpath = "//*[@id=\'sim-detail\']/form/div[4]/select")
	    public WebElement dropTipoTasa;

	    @FindBy(name = "comboTipoProducto")
	    public WebElement  dropTipoProducto;
	    
	    @FindBy(name = "checkSeguroDesempleo")
	    public WebElement checksegurodesempleo;
	    	
	    @FindBy(name = "textPlazoInversion")
	    public WebElement  plazoprestamo;
	    
	    @FindBy(name = "textValorPrestamo")
	    public WebElement  valoraprestar;

	    @FindBy(css = ".button-row:nth-child(8)")
	    public WebElement btnSimular;
	    
	    @FindBy(xpath="//td[contains(text(),'17.46%')]")
	    public WebElement tasaEfectivaAnual;
	    
	    @FindBy(xpath="//td[contains(text(),'1.35%')]")
	    public WebElement tasaMesVencido;
	    
	    @FindBy(xpath="//*[@id=\'sim-results\']/div[1]/table/tbody/tr[3]/td[3]")
	    public WebElement cuotaMensual;
	    
	    @FindBy(xpath="//*[@id=\'sim-results\']/div[1]/table/tbody/tr[4]/td[2]")
	    public WebElement seguroVidaAsociado;
	    
	    @FindBy(xpath="//*[@id=\'sim-results\']/div[1]/table/tbody/tr[5]/td[2]")
	    public WebElement seguroDesempleo;
	    
	    @FindBy(xpath="//*[@id=\'sim-results\']/div[1]/table/tbody/tr[6]/td[2]")
	    public WebElement cuotaMensualSeguro;
	    
	    @FindBy(xpath="//td[contains(text(),'24 meses')]")
	    public WebElement plazomes;
	    	 
	    public void simulaCuota(String value0) {
	        Select selectsimularcuota = new Select(element(dropSimularCuota));
	        selectsimularcuota.selectByVisibleText(value0);
	    }
	    public void ingresar_fecha_nacimiento(){
	          cajaFechaNacimiento.click();
	         cajaFechaNacimiento.sendKeys("1985-07-26");
	         cajaFechaNacimiento.sendKeys(Keys.TAB);
	  	    }
	    public void ingresa_tipo_tasa(String ttasa) {
	    	Select selecttipotasa = new Select(element(dropTipoTasa));
	        selecttipotasa.selectByVisibleText(ttasa);
	        dropTipoTasa.sendKeys(Keys.TAB);    	
	    }
	    public void ingresa_tipo_producto(String tproducto) {
	    	  System.out.println("Entre a la  tipo producto");
	    	  Select selecttipoproducto = new Select(element(dropTipoProducto));
	    	  selecttipoproducto.selectByVisibleText(tproducto);
	    	 dropTipoProducto.sendKeys(Keys.TAB);  	
	    }
	    public void checkbox()  {
	    	System.out.println("Ingrese a check");
	  	    	checksegurodesempleo.click();
	    	checksegurodesempleo.sendKeys(Keys.TAB);
        }
	    public void plazo_prestamo()  {
	    	System.out.println("Ingrese a plazo prestamo");
	      	plazoprestamo.click();
	    	plazoprestamo.sendKeys("24");
	    	plazoprestamo.sendKeys(Keys.TAB);
	    }
	    public void valor_deseado() {
	    	valoraprestar.click();
	    	valoraprestar.sendKeys("10000000");
	    	valoraprestar.sendKeys(Keys.TAB);
	  	}
	    public void ejeuctar_simulacion() {
	    	btnSimular.click();
	    }
	    public void resultado(double dato) { 			
	    		    	
	        double tEA = tasaEfectivaAnual();
	    	double tMV = tasaMesVencido() / 100;
	    	double plazo = plazomes();
	    	double valorCredito = retornarValorCredito(dato);
	    	double seguro = seguroVidaAsociado();
	    	double cuota = valorCredito * ((tMV * Math.pow(1 + tMV, plazo)) / (Math.pow(1 + tMV, plazo)- 1));
	    	double cuota_Seguro = (seguro + cuota);
	    	String pagocuota =cuotaMensual.getText();
    		assertThat.(pagocuota, containText(cuota_Seguro));
	    }
 }







