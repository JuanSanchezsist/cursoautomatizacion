package com.bancolombia2.formacion.definition;

import com.bancolombia2.formacion.steps.SimuladorStep;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class SimuladorDefinition {

	    @Steps
	    SimuladorStep simuladorSteps;

	    @Given("^Cliente ingresa a pantalla simulacion credito$")
	    public void cliente_ingresa_a_pantalla_simulacion_credito() {
	        simuladorSteps.ingresar_pagina_simulacro();

	    }

	    @When("^Simula valor de la cuota$")
	    public void simula_valor_de_la_cuota() {
	        simuladorSteps.diligenciar_formulario();
	    }

	    @Then("^Cliente debe ver informacion de la consulta$")
	    public void cliente_verifica_la_información() {
	    	simuladorSteps.validarresult();

	    }
	

}
