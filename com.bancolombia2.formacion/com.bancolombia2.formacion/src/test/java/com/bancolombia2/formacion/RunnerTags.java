package com.bancolombia2.formacion;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions (features = "src/test/resources/features/Bancolombia2/Forms/Simulador_cuota.feature", tags = "@Simulador")

public class RunnerTags {

}
