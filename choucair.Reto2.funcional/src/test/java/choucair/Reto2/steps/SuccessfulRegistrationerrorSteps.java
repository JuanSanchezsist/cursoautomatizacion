package choucair.Reto2.steps;

import choucair.Reto2.pageobjects.SuccessfulRegistrationerrorPageObjects;
import choucair.capacitacion.funcional.pageobjects.SuccessfulRegistrationPageObjects;
import net.thucydides.core.annotations.Step;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;


public class SuccessfulRegistrationerrorSteps {
	
	SuccessfulRegistrationerrorPageObjects successfulRegistrationerrorPageObjects;
	
	
	public void OpenUrl() {
		successfulRegistrationerrorPageObjects.open();
						
	}


	public void VerifyRegister() {
		successfulRegistrationerrorPageObjects.VerifyRegister();
				
	}

	public void fillcorrectform(String firstname, String firstname2, String lastname, String country, String city, String phonenumber,
			String email, String language, String platfrom, String type, String currency, String leverage,
			String amount, String password, String passwordconfirmation, String acceptnews, String openaccount,
			String verifiedcreated) {
		successfulRegistrationerrorPageObjects.fillcorrectform(firstname, firstname2, lastname, country, city, phonenumber, email, language,  platfrom, type, currency,	leverage, amount, password, passwordconfirmation, acceptnews, openaccount, verifiedcreated);
		
		firstname="ÑOÑOª";
		firstname2="Jair";
		lastname="Sanchez";
		city="Bogota";
	    phonenumber="31351275698";
	 	email="jmsanchez@choucairtesting.com";
		password="pxQQpP09";
		passwordconfirmation="pxQQpP09";	
		
	}

}
