package choucair.Reto2.steps;

import choucair.Reto2.pageobjects.SuccessfulRegistrationPageObjects;
import net.thucydides.core.annotations.Step;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class SuccessfulRegistrationSteps {
	
	
	SuccessfulRegistrationPageObjects successfulRegistrationPageObjects;


	
	@Step
	public void OpenUrlXm() {
		successfulRegistrationPageObjects.open();
			}
	
	@Step
	public void FillForm (String firstname, String lastname, String country, String city, String phonenumber,String email, String language, String platfrom, String type, String currency,
			String leverage, String amount, String password, String passwordconfirmation, String acceptnews, String openaccount, String verifiedcreated) {
		successfulRegistrationPageObjects.FillForm(firstname, lastname, country, city, phonenumber, email, language,  platfrom, type, currency,	leverage, amount, password, passwordconfirmation, acceptnews, openaccount, verifiedcreated);
		
		firstname="Juan";
		lastname="Sanchez";
		city="Bogota";
	    phonenumber="31351275698";
	 	email="jmsanchez@choucairtesting.com";
		password="pxQQpP09";
		passwordconfirmation="pxQQpP09";
		
	}
	@Step
	public void VerifyAccount() {
		successfulRegistrationPageObjects.VerifyAccount();
			}




}
