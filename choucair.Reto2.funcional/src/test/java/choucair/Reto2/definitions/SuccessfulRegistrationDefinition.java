package choucair.Reto2.definitions;

import choucair.Reto2.steps.SuccessfulRegistrationSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class SuccessfulRegistrationDefinition {
	
	@Steps
	SuccessfulRegistrationSteps successfulRegistrationSteps;
	
		
	@Given("^that you need to register a demo account$")
	public void that_you_need_to_register_a_demo_account() throws Throwable {
		successfulRegistrationSteps.OpenUrlXm();
		
	}


	@When("^diligence the form$")
	public void diligence_the_form(String firstname, String lastname, String country, String city, String phonenumber,String email, String language, String platfrom, String type, String currency,
			String leverage, String amount, String password, String passwordconfirmation, String acceptnews, String openaccount, String verifiedcreated)  {
		successfulRegistrationSteps.FillForm(firstname, lastname, country, city, phonenumber, email, language,  platfrom, type, currency,	leverage, amount, password, passwordconfirmation, acceptnews, openaccount, verifiedcreated);
		
		}
	@Then("^he verifies that the opening message is displayed on the screen$")
	public void he_verifies_that_the_opening_message_is_displayed_on_the_screen() throws Throwable {
		successfulRegistrationSteps.VerifyAccount();
		
	}

}
