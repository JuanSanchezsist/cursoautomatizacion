package choucair.Reto2.definitions;

import choucair.Reto2.steps.SuccessfulRegistrationerrorSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class SuccessfulRegistrationerrorDefinition {
	
	@Steps
	SuccessfulRegistrationerrorSteps successfulRegistrationerrorSteps;
	
	
	@Given("^that you need to register account$")
	public void that_you_need_to_register_account() {
		successfulRegistrationerrorSteps.OpenUrl();
		
	    
	}

	@When("^diligence the form with error$")
	public void diligence_the_form_with_error(String firstname, String firstname2, String lastname, String country, String city, String phonenumber,String email, String language, String platfrom, String type, String currency,
			String leverage, String amount, String password, String passwordconfirmation, String acceptnews, String openaccount, String verifiedcreated)   {
		successfulRegistrationerrorSteps.fillcorrectform(firstname, lastname, country, city, phonenumber, email, language,  platfrom, type, currency,	leverage, amount, password, passwordconfirmation, acceptnews, openaccount, verifiedcreated);
		
	}

	@Then("^he verifies that the opening message is displayed$")
	public void he_verifies_that_the_opening_message_is_displayed()  {
		successfulRegistrationerrorSteps.VerifyRegister();
	    
	}
	
	

}
