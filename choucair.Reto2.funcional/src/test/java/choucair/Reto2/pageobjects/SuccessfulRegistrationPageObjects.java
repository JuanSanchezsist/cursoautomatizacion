package choucair.Reto2.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import org.openqa.selenium.Keys;

@DefaultUrl ("https://www.xm.com/register/account/demo?lang=es")

public class SuccessfulRegistrationPageObjects extends PageObject {
	
	        @FindBy(xpath ="//*[@id='first_name']")
			public WebElementFacade txtfirstname;
			@FindBy(xpath ="//*[@id='last_name']")
			public WebElementFacade  txtlastname;
	        @FindBy(xpath ="//*[@id=\'country\']")
			public WebElementFacade dropcountry;
			@FindBy(xpath ="//*[@id=\'city\']/option[44]")
			public WebElementFacade  txtcity;
			@FindBy(xpath ="//*[@id=\'phone_number\']")
			public WebElementFacade txtphonenumber;
			@FindBy(xpath ="//*[@id=\'email\']")
			public WebElementFacade  txtemail;
		    @FindBy(xpath ="//*[@id=\'preferred_language\']/option[16]")
			public WebElementFacade droplanguage;
			@FindBy(xpath = "//*[@id=\'trading_platform_type\']/option[2]")
			public WebElementFacade  dropplatfrom;
			@FindBy(xpath ="//*[@id='account_type']/option[1]")
			public WebElementFacade  droptype;
			@FindBy(xpath ="//*[@id=\'account_currency\']/option[2]")
			public WebElementFacade dropcurrency;
			@FindBy(xpath ="//*[@id=\'account_leverage\']/option[1]")
			public WebElementFacade  dropleverage;
			@FindBy(xpath = "//*[@id=\'investment_amount\']/option[9]")
			public WebElementFacade  dropamount;
			@FindBy(xpath ="//*[@id='account_password']")
			public WebElementFacade  txtpassword;
			@FindBy(xpath ="//*[@id=\'account_password_confirmation\']")
			public WebElementFacade  txtpasswordconfirmation;
			@FindBy(xpath ="//*[@id='demo-form']/div[3]/div[14]/div/div/div/label")	
			public WebElementFacade  chckbxacceptnews;
			@FindBy(xpath ="//*[@id='submit-btn']")
			public WebElementFacade  btnopenaccount;
			@FindBy(xpath ="//*[@id='top']/div[1]/div/div/div[1]/div[1]/div/div[2]/div/div/div[2]/h2")
			public WebElementFacade  verifiedcreated;
			
			
	public void FillForm(String firstname, String lastname, String country, String city, String phonenumber,String email, String language, String platfrom, String type, String currency,
			String leverage, String amount, String password, String passwordconfirmation, String acceptnews, String openaccount, String verifiedcreated) {
		
	
		txtfirstname.sendKeys(firstname);
		txtlastname.sendKeys(lastname);
		dropcountry.click();
		txtphonenumber.sendKeys(phonenumber);
		txtemail.sendKeys(email);
		droplanguage.click();
		dropplatfrom.click();
		droptype.click();
		dropcurrency.click();
		dropleverage.click();
		dropamount.click();
		txtpassword.sendKeys(password);
		txtpasswordconfirmation.sendKeys(passwordconfirmation);
		chckbxacceptnews.click();
		btnopenaccount.click();		

	}

	public void VerifyAccount() {
		
		String mensaje = verifiedcreated.getText();
		assertThat(mensaje, containsString("Enhorabuena por la apertura de una Cuenta Demo de XM"));

	}
	

	
	
}
