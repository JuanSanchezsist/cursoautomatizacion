import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)

@CucumberOptions (features = "src/test/resources/features/Simulador_cuota.feature", tags = "@Simulador")

public class Runner {
	
}
