package simuladorcuota.page;


import ch.qos.logback.core.net.SyslogOutputStream;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.sql.Driver;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;


@DefaultUrl("https://www.grupobancolombia.com/wps/portal/personas/productos-servicios/creditos/consumo/libre-inversion/simulador-credito-consumo")

public class SimuladorPage extends PageObject{
	
	   

    @FindBy (xpath = "//*[@id=\'sim-detail\']/form/div[2]/select")
    public WebElementFacade dropSimularCuota;

    
   // @FindBy(xpath = "//*[@id=\'sim-detail\']/form/div[2]/p[1]")
   // public  WebElement mensajesimulador;


//*[@id="dp1590604734427"]
    @FindBy(xpath = "//form[@name='creditoconsumoForm']/descendant::input[@name='dateFechaNacimiento']")
    public  WebElement cajaFechaNacimiento;

    @FindBy(css = "select.form-control.ng-dirty.ng-valid.ng-valid-required:nth-child(2)")
    WebElementFacade dropTipoTasa;

    @FindBy(css = ".button-row:nth-child(8)select.form-control.ng-pristine.ng-invalid.ng-invalid-required:nth-child(2)")
    WebElementFacade dropTipoProducto;

    @FindBy(css = ".button-row:nth-child(8)")
    WebElementFacade btnSimular;

    public void simulaCuota(String value0) {
        System.out.println("ingrso al 1 campo");
    	        Select selectsimularcuota = new Select(element(dropSimularCuota));
        selectsimularcuota.selectByVisibleText(value0);
       /* String texto = "Este simulador te permite identificar el valor de la cuota que debes pagar cada mes, según el monto que deseas prestar y el plazo que solicites.";
        System.out.println(texto);
        String strMensaje = mensajesimulador.getText();
        assertThat(texto, getTex(strMensaje));
        System.out.println(strMensaje);
        System.out.println(texto);*/
    }
    
    public void ingresar_fecha_nacimiento(){
         System.out.println("Entre a la  fecha");
         cajaFechaNacimiento.click();
         try {
             Thread.sleep(10000);
         } catch (InterruptedException e) {
             e.printStackTrace();
         }
         //String date = "1985-07-26";
         cajaFechaNacimiento.sendKeys("1985-07-26");
       // cajaFechaNacimiento.sendKeys(Keys.TAB);

         try {
             Thread.sleep(10000);
         } catch (InterruptedException e) {
             e.printStackTrace();
         }
    }



    /*        assertThat(textoCampo, containsString(strMensaje));

        assertThat(strMensaje, containsString(textoCampo));
        System.out.println(strMensaje);
        System.out.println(textoCampo);

*/


  }












