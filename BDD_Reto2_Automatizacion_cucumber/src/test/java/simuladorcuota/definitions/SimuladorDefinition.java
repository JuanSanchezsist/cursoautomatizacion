package simuladorcuota.definitions;

import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.thucydides.core.annotations.Steps;
import simuladorcuota.steps.SimuladorStep;


public class SimuladorDefinition {

    @Steps
    SimuladorStep simuladorSteps;

    @Dado("^Cliente ingresa a pantalla simulacion credito$")
    public void cliente_ingresa_a_pantalla_simulacion_credito() {
        simuladorSteps.ingresar_pagina_simulacro();

    }

    @Cuando("^Simula valor de la cuota$")
    public void simula_valor_de_la_cuota() {
        simuladorSteps.diligenciar_formulario();
    }

    @Entonces("^Cliente debe ver informacion de la consulta$")
    public void cliente_verifica_la_información() {

    }
}
