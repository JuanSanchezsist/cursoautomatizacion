package simuladorcuota.steps;



import net.thucydides.core.annotations.Step;
import simuladorcuota.page.SimuladorPage;


public class SimuladorStep {

    SimuladorPage simuladorPage;

    @Step
    public void ingresar_pagina_simulacro() {
        simuladorPage.open();
    }
    @Step
    public void  diligenciar_formulario( ){
        simuladorPage.simulaCuota("Simula tu Cuota");
        simuladorPage.ingresar_fecha_nacimiento();


}



}
