#language:es
Característica: Verificar el funcionamiento de la pantalla de simulación de créditos expuesta
  por el grupo Bancolombia, en cuanto a la presentación de los
  valores calculados, campos: “Valor de la Cuota”.

@Simulador
  Escenario: Simular la cuota en Tasa Fija para un crédito Libre Inversión,
  con plazos (36, 48 y 60 meses) por un valor de 10 millones

    Dado Cliente ingresa a pantalla simulacion credito
    Cuando Simula valor de la cuota
    Entonces Cliente debe ver informacion de la consulta